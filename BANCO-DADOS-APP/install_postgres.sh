# /bin/bash
# Instalando e configurando o Postgres SQL com as bases de dados para produ��o e homologa��o

apt-get install postgresql -y

######### Configurando base de dados para o Homologacao e Producao #########
su -c psql -s /bin/bash postgres << EOF
CREATE ROLE uhomologacao LOGIN ENCRYPTED PASSWORD 'uHomologacao' NOINHERIT VALID UNTIL 'infinity';
CREATE ROLE uproducao LOGIN ENCRYPTED PASSWORD 'uProducao' NOINHERIT VALID UNTIL 'infinity';
CREATE DATABASE homologacao WITH ENCODING='UTF8' OWNER=uhomologacao;
CREATE DATABASE producao WITH ENCODING='UTF8' OWNER=uproducao;
ALTER user postgres WITH ENCRYPTED PASSWORD 'postgres';
EOF

service postgresql stop

su -s /bin/bash postgres << EOF
cd /var/lib/postgresql
wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/BANCO-DADOS-APP/configs/postgresql/pg_hba.conf
chmod 644 pg_hba.conf
wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/BANCO-DADOS-APP/configs/postgresql/postgresql.conf
chmod 640 postgresql.conf
EOF

mv /etc/postgresql/9.4/main/pg_hba.conf /etc/postgresql/9.4/main/pg_hba.conf.backup
mv /var/lib/postgresql/pg_hba.conf /etc/postgresql/9.4/main/

mv /etc/postgresql/9.4/main/postgresql.conf /etc/postgresql/9.4/main/postgresql.conf.backup
mv /var/lib/postgresql/postgresql.conf /etc/postgresql/9.4/main/
