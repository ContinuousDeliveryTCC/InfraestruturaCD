# /bin/bash
# Configura máquina FIREWALL para servir de roteador de Internet na Infraestrura de Entrega Contínua
# Script escrito por Bruno Nogueira de Oliveira

# Configura o Resolvedor de nomes para pegar o Default
echo "nameserver 127.0.0.1" > /etc/resolv.conf

# Habilitando encaminhamento entre a interface eth1 e eth0
echo "1" > /proc/sys/net/ipv4/ip_forward

# Dropando todas as regras do iptables
iptables -F INPUT
iptables -F OUTPUT
iptables -F FORWARD
iptables -t nat -F

# Mascarando ip de origem dos pacotes originados na rede interna. Protege os ambientes internos
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE

# Encaminhando todas as requisições direcionadas à porta 5432 para a máquina de Banco de Dados
# EXEMPLO AINDA NAO HABILITADO - Isso é bom para quando for preciso acessar o SGBD externamente (HABILITAR COM CUIDADO)

# iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 5432 -j DNAT --to-destination 10.20.27.6:5432
# iptables -t nat -A POSTROUTING -o eth1 -p tcp --dport 5432 -j SNAT --to 10.20.27.1


# Encaminhando todas as requisições feitas na porta 8080, 8081, 9000, etc, para o servidor que deve responder
# EXEMPLO AINDA NAO HABILITADO - conforme novos ambientes forem surgindo, duplicar a regra e apontar a porta e o IP da máquina interna correta.
iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 8080 -j DNAT --to-destination 10.20.27.5:8080
iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 8081 -j DNAT --to-destination 10.20.27.5:8081
iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 9000 -j DNAT --to-destination 10.20.27.5:9000
iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 81 -j DNAT --to-destination 10.20.27.3:8080
iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 80 -j DNAT --to-destination 10.20.27.2:8080
