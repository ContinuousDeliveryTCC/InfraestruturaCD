# Script orquestrador
# Suba a máquina virtual zerada e execute esse Script em modo ROOT

# Baixa arquivo de configuração do Firewall na máquina
wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/FIREWALL/firewall.sh
wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/FIREWALL/firewall.sh.switch

# Cria pasta do Firewall na máquina
mkdir /etc/firewall
mv firewall.sh /etc/firewall/
mv firewall.sh.switch /etc/firewall

# Da permissao para o arquivo
chmod +x /etc/firewall/firewall.sh
chmod +x /etc/firewall/firewall.sh.switch

# Configura o Script para ser executado na inicialização da máquina
# Primeiro faz backup do rc.local padrão. Depois baixa o template do Gitlab
mv /etc/rc.local /etc/rc.local.backup
wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/FIREWALL/rc.local
mv rc.local /etc/
chmod +x /etc/rc.local

# Baixa arquivo /etc/network/interface
wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/FIREWALL/interfaces
mv /etc/network/interfaces /etc/network/interfaces.backup
mv interfaces /etc/network/

# Instala o Bind para resolver nomes
wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/FIREWALL/install_bind9.sh
chmod +x install_bind9.sh
sh install_bind9.sh
rm install_bind9.sh

# Instala o sudo e da permissal geral
wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/FIREWALL/install_sudo.sh
chmod +x install_sudo.sh
sh install_sudo.sh
rm install_sudo.sh

wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/FIREWALL/configs/release.sh
mv release.sh /home/debian/release.sh
chmod +x /home/debian/release.sh

# Pequena gambiarra para habilitar a execu��o de Script SSH via Jenkins
echo "KexAlgorithms diffie-hellman-group1-sha1,curve25519-sha256@libssh.org,ecdh-sha2-nistp256,ecdh-sha2-nistp384,ecdh-sha2-nistp521,diffie-hellman-group-exchange-sha256,diffie-hellman-group14-sha1" >> /etc/ssh/sshd_config

# reinicia a máquina
shutdown -h now
