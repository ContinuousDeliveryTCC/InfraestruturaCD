# !/bin/bash
# Instala e configura o resolvedor de nomes da aplica��o

apt-get install bind9 -y

wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/FIREWALL/configs/named.conf.local
mv /etc/bind/named.conf.local /etc/bind/named.conf.local.backup
mv named.conf.local /etc/bind/

wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/FIREWALL/configs/pfc2.si.ufg.br.dns
wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/FIREWALL/configs/pfc2.si.ufg.br.dns.switch
mv pfc2.si.ufg.br.dns /etc/bind/
mv pfc2.si.ufg.br.dns.switch /etc/bind/

service bind9 reload
