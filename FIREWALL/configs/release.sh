# !/bin/bash
# Arquivo SH que troca as regras de IPTABLES para implementar o Blue-green

mv /etc/firewall/firewall.sh /etc/firewall/firewall.sh.bkp
mv /etc/firewall/firewall.sh.switch /etc/firewall/firewall.sh
mv /etc/firewall/firewall.sh.bkp /etc/firewall/firewall.sh.switch
sh /etc/firewall/firewall.sh

mv /etc/bind/pfc2.si.ufg.br.dns /etc/bind/pfc2.si.ufg.br.dns.bkp
mv /etc/bind/pfc2.si.ufg.br.dns.switch /etc/bind/pfc2.si.ufg.br.dns
mv /etc/bind/pfc2.si.ufg.br.dns.bkp /etc/bind/pfc2.si.ufg.br.dns.switch
service bind9 reload
