# Script orquestrador
# Suba a máquina virtual zerada e execute esse Script em modo ROOT

# Configura o Script para ser executado na inicialização da máquina
# Primeiro faz backup do rc.local padrão. Depois baixa o template do Gitlab
mv /etc/rc.local /etc/rc.local.backup
wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/BANCO-DADOS/rc.local
mv rc.local /etc/
chmod +x /etc/rc.local

# Baixa arquivo /etc/network/interface
wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/BANCO-DADOS/interfaces
mv /etc/network/interfaces /etc/network/interfaces.backup
mv interfaces /etc/network/

# Baixa e executa o instalador do Postgres;
wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/BANCO-DADOS/install_postgres.sh
sh install_postgres.sh
rm install_postgres.sh

# Desligo a m�quina para ela reiniciar com as configura��es carregadas
shutdown -h now
