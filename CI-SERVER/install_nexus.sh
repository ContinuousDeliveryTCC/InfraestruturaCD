# /bin/bash
# Instalador do Nexus

wget http://download.sonatype.com/nexus/3/nexus-3.0.0-03-unix.tar.gz
tar xvzf nexus-3.0.0-03-unix.tar.gz

# Baixando configuraçaõ especifica do Nexus do Gitlab e sobrescrevendo
wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/CI-SERVER/configs/nexus/nexus
mv nexus-3.0.0-03/bin/nexus nexus-3.0.0-03/bin/nexus-backup
mv nexus nexus-3.0.0-03/bin/
mv nexus-3.0.0-03 /opt/
ln -s /opt/nexus-3.0.0-03/ /opt/nexus
chmod 777 -R /opt/nexus

# Adicionando um novo usuário
adduser -home /home/nexus -system -shell /bin/bash nexus

# Criando grupo para o usuário
groupadd nexus-admin

# Adicionando usuário ao grupo
usermod -a -G nexus-admin nexus

echo "NEXUS_HOME=\"/opt/nexus\"" > /home/nexus/.bashrc
echo "run_as_user=\"nexus\"" > /opt/nexus/bin/nexus.rc

ln -s /opt/nexus/bin/nexus /etc/init.d/nexus

update-rc.d nexus defaults
service nexus start
