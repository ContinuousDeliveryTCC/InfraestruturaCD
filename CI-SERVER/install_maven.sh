# /bin/bash
# Instalando e configurando o Maven na máquina

wget http://mirror.nbtelecom.com.br/apache/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.tar.gz
tar -zxvf apache-maven-3.3.9-bin.tar.gz
mv apache-maven-3.3.9/ /opt/
rm apache-maven-3.3.9-bin.tar.gz
echo "export M3_HOME=/opt/apache-maven-3.3.9" >> ~/.bashrc
echo "export M3=\$M3_HOME/bin" >> ~/.bashrc
echo "export PATH=\$M3:\$PATH" >> ~/.bashrc
echo "export JAVA_HOME=/usr/lib/jvm/java-8-oracle" >> ~/.bashrc
echo "export PATH=\$JAVA_HOME/bin:\$PATH" >> ~/.bashrc
