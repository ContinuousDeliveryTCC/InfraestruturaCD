# Script orquestrador
# Suba a máquina virtual zerada e execute esse Script em modo ROOT

# Configura o Script para ser executado na inicialização da máquina
# Primeiro faz backup do rc.local padrão. Depois baixa o template do Gitlab
mv /etc/rc.local /etc/rc.local.backup
wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/CI-SERVER/rc.local
mv rc.local /etc/
chmod +x /etc/rc.local

# Baixa arquivo /etc/network/interface
wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/CI-SERVER/interfaces
mv /etc/network/interfaces /etc/network/interfaces.backup
mv interfaces /etc/network/

# Instalando o Java 7 e 8 da Oracle na máquina.
# Baixa o Script do Gitlab, dá permissão, executa e depois apaga o arquivo baixado.
wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/CI-SERVER/install_java_oracle.sh
chmod +x install_java_oracle.sh
sh install_java_oracle.sh
rm install_java_oracle.sh

# Instalando o Jenkins na máquina.
# Baixa o Script do Gitlab, dá permissão, executa e depois apaga o arquivo baixado.
wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/CI-SERVER/install_jenkins.sh
chmod +x install_jenkins.sh
sh install_jenkins.sh
rm install_jenkins.sh

# Instalando o Git na máquina.
# Baixa o Script do Gitlab, dá permissão, executa e depois apaga o arquivo baixado.
wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/CI-SERVER/install_git.sh
chmod +x install_git.sh
sh install_git.sh
rm install_git.sh

# Instalando o Maven na máquina
wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/CI-SERVER/install_maven.sh
chmod +x install_maven.sh
sh install_maven.sh
rm install_maven.sh

# Instalando o Nexus na máquina
wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/CI-SERVER/install_nexus.sh
chmod +x install_nexus.sh
sh install_nexus.sh
rm install_nexus.sh


# Instalando o Sonar na maquina
wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/CI-SERVER/install_sonar.sh
chmod +x install_sonar.sh
sh install_sonar.sh
rm install_sonar.sh

# Desliga a máquina
shutdown -h now
