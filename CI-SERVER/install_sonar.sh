# /bin/bash
# Instalador do Sonar

echo "deb http://downloads.sourceforge.net/project/sonar-pkg/deb binary/" >> /etc/apt/sources.list
apt-get update
apt-get install sonar --force-yes -y

wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/CI-SERVER/configs/sonar/sonar.properties
mv /opt/sonar/conf/sonar.properties /opt/sonar/conf/sonar.properties.backup
mv sonar.properties /opt/sonar/conf/
update-rc.d sonar defaults
