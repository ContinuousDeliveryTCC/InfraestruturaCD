InfraestruturaCD

Repositório que contém os Scripts que configuram as máquinas dos servidores de Entrega Contínua;

Os scripts gerados aqui foram escritos para serem executados em máquinas com Sistema Operacional Debian, versão 8.1;

A imagem da máquina virutal encontra-se no diretório raiz, com o nome "Debian CL - Base.ova" e deve ser importada no Virtual Box.

Dados dessa máquina virtual:  
Usuário: debian
Senha: 123456

Usuário: root
Senha: bruno2016@