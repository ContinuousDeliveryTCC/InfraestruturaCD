# Script orquestrador
# Suba a máquina virtual zerada e execute esse Script em modo ROOT

# Configura o Script para ser executado na inicialização da máquina
# Primeiro faz backup do rc.local padrão. Depois baixa o template do Gitlab
mv /etc/rc.local /etc/rc.local.backup
wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/PRODUCAO/rc.local
mv rc.local /etc/
chmod +x /etc/rc.local

# Baixa arquivo /etc/network/interface
wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/PRODUCAO/interfaces
mv /etc/network/interfaces /etc/network/interfaces.backup
mv interfaces /etc/network/

# Baixa e executa o instalador do Java da Oracle;
wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/PRODUCAO/install_java_oracle.sh
sh install_java_oracle.sh
rm install_java_oracle.sh


# Baixa e executa o instalador do Postgres;
wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/PRODUCAO/install_tomcat.sh
sh install_tomcat.sh
rm install_tomcat.sh

wget https://gitlab.com/ContinuousDeliveryTCC/ConfiguracoesCD/raw/master/db/db.hikari.properties.homologacao
wget https://gitlab.com/ContinuousDeliveryTCC/ConfiguracoesCD/raw/master/db/db.hikari.properties.producao
mkdir -p /usr/share/tomcat8/xyz/config/db
mv db.hikari.properties.producao /usr/share/tomcat8/xyz/config/db/db.hikari.properties
mv db.hikari.properties.homologacao /usr/share/tomcat8/xyz/config/db/db.hikari.properties.switch
chmod 777 -R /usr/share/tomcat8/xyz/

wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/PRODUCAO/release.sh
mv release.sh /home/debian/release.sh
chmod +x /home/debian/release.sh

# Pequena gambiarra para habilitar a execu��o de Script SSH via Jenkins
echo "KexAlgorithms diffie-hellman-group1-sha1,curve25519-sha256@libssh.org,ecdh-sha2-nistp256,ecdh-sha2-nistp384,ecdh-sha2-nistp521,diffie-hellman-group-exchange-sha256,diffie-hellman-group14-sha1" >> /etc/ssh/sshd_config

# Instala o sudo e da permissal geral
wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/PRODUCAO/install_sudo.sh
chmod +x install_sudo.sh
sh install_sudo.sh
rm install_sudo.sh

# Desligo a m�quina para ela reiniciar com as configura��es carregadas
shutdown -h now
