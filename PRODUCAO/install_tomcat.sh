# /bin/bash
# Instalador do Tomcat versao 8

apt-get install tomcat8 -y

wget http://ftp.unicamp.br/pub/apache/tomcat/tomcat-8/v8.0.36/bin/apache-tomcat-8.0.36.tar.gz
tar -vzxf apache-tomcat-8.0.36.tar.gz
cp -R apache-tomcat-8.0.36/webapps/* /var/lib/tomcat8/webapps/
rm -rf apache*

wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/PRODUCAO/configs/tomcat-users.xml
mv /var/lib/tomcat8/conf/tomcat-users.xml /var/lib/tomcat8/conf/tomcat-users.xml.backup
mv tomcat-users.xml /var/lib/tomcat8/conf/

wget https://gitlab.com/ContinuousDeliveryTCC/InfraestruturaCD/raw/master/PRODUCAO/configs/tomcat8
rm /etc/init.d/tomcat8
mv tomcat8 /etc/init.d/
chmod 755 /etc/init.d/tomcat8

service tomcat8 restart
